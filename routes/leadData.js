var express = require('express');
var router = express.Router();
const clientCtrl = require("../controllers/leaddata")
let middleware = require('../middleware');
let storage = require('../middleware/storage');


router.get('/', middleware.isAuthenticated, clientCtrl.clients);
router.post('/', middleware.isAuthenticated, clientCtrl.createClient);
router.get('/count', middleware.isAuthenticated, clientCtrl.clientsCount);
router.post('/array', middleware.isAuthenticated, clientCtrl.createClientArray);

router.get('/inprogress', middleware.isAuthenticated, clientCtrl.clientsInProgress);
router.post('/inprogress', middleware.isAuthenticated, clientCtrl.createInProgressClient);
router.put('/inprogress', middleware.isAuthenticated, clientCtrl.updateInProgressClient);

router.delete('/inprogress/:id', middleware.isAuthenticated, clientCtrl.deleteClient);
router.get('/inprogress/count', middleware.isAuthenticated, clientCtrl.clientsInProgressCount);

router.get('/final', middleware.isAuthenticated, clientCtrl.clientsFinal);
router.post('/final', middleware.isAuthenticated, clientCtrl.createFinalClient);
router.delete('/final/:id', middleware.isAuthenticated, clientCtrl.deleteClient);
router.get('/final/count', middleware.isAuthenticated, clientCtrl.clientsFinalCount);

router.get('/:id', middleware.isAuthenticated, clientCtrl.clientDetails);
router.delete('/:id', middleware.isAuthenticated, clientCtrl.deleteClient);

module.exports = router;
