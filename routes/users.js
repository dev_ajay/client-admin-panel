var express = require('express');
var router = express.Router();
const userCtrl = require("../controllers/users")
let middleware = require('../middleware');
let storage = require('../middleware/storage');

router.get('/', middleware.isAuthenticated, userCtrl.usersList);

router.get('/me', middleware.isAuthenticated, userCtrl.me);
router.get('/search', middleware.isAuthenticated, userCtrl.searchOnlyUsers);
router.get('/search-all/:search', middleware.isAuthenticated, userCtrl.searchAll);
router.get('/:id', middleware.isAuthenticated, userCtrl.userDetails);

router.put('/:id', middleware.isAuthenticated,  storage.uploadImage.single('images'), userCtrl.updateUser);

module.exports = router;
