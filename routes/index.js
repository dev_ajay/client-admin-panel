var express = require('express');
var router = express.Router();
const indexCtrl = require("../controllers/index")
let middleware = require('../middleware');
let storage = require('../middleware/storage');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register', indexCtrl.register);

router.post('/login', indexCtrl.login);

router.post('/forgot-password', indexCtrl.forgetPassword)

router.post('/reset-password', indexCtrl.resetPassword);

router.post('/change-password', middleware.isAuthenticated, indexCtrl.changePassword);

module.exports = router;
