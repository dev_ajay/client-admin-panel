const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const { Schema } = mongoose;

const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    unique: false,
    required: false,
    trim: true,
  },
  phone: {
    type: String,
    default: "",
    unique: false,
    required: false
  }
}, { timestamps: true });

// Authentication

UserSchema.statics.authenticate = (email, password, callback) => {
  User.findOne({ email: email })
    .exec(function (error, user) {
      if (user) {
        if (error) {
          return callback(error);
        } else if (!email) {
          var err = new Error('Email not found');
          err.status = 401;
          return callback(err)
        }
        bcrypt.compare(password, user.password, function (error, result) {
          if (result === true) {
            return callback(null, user);
          } else {
            return callback();
          }
        })
      } else {
        var err = new Error('Email or password you have entered is wrong!');
        err.status = 401;
        return callback(err)
      }
    });
};

UserSchema.methods.toJSON = function () {
  var obj = this.toObject();
  delete obj.password;
  return obj;
}

var User = mongoose.model('User', UserSchema);
module.exports = User;
