const mongoose = require('mongoose');
const { Schema } = mongoose;

const ClientSchema = new Schema({
  status: {
    type: String,
    required: true,
    trim: true,
    default: 'client'
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  name: {
    type: String,
    unique: false,
    required: false,
    trim: true,
  },
  phone: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  course: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  date: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  dob: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  qualifications: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  yearOfPassing: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  counselor: {
    type: String,
    default: "",
    unique: false,
    required: false
  },

  reference: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  feedback: {
    type: String,
    default: "",
    unique: false,
    required: false
  },

  remarks: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  remarks1: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  remarks2: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  fee: {
    type: Number,
    unique: false,
    required: false
  },
  scheduleCall: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  dateOfJoining: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  details: {
    type: String
  }
}, { timestamps: true });

var Client = mongoose.model('Client', ClientSchema);
module.exports = Client;
