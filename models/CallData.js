const mongoose = require('mongoose');
const { Schema } = mongoose;

const CallSchema = new Schema({
  status: {
    type: String,
    required: true,
    trim: true,
    default: 'client'
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  name: {
    type: String,
    unique: false,
    required: false,
    trim: true,
  },
  phone: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  course: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  visit_date: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  contact_person: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  remark: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  fee: {
    type: Number,
    unique: false,
    required: false
  },
  scheduleCall: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  dateOfJoining: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  details: {
    type: String
  }
}, { timestamps: true });

var CallData = mongoose.model('CallData', CallSchema);
module.exports = CallData;
