const mongoose = require('mongoose');
const { Schema } = mongoose;

const LeadSchema = new Schema({
  status: {
    type: String,
    required: true,
    trim: true,
    default: 'client'
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  name: {
    type: String,
    unique: false,
    required: false,
    trim: true,
  },
  phone: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  course: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  date: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  campaign_name: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  platform: {
    type: String,
    default: "",
    unique: false,
    required: false
  },

  feedback: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  fee: {
    type: Number,
    unique: false,
    required: false
  },
  scheduleCall: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  dateOfJoining: {
    type: String,
    default: "",
    unique: false,
    required: false
  },
  details: {
    type: String
  }
}, { timestamps: true });

var LeadData = mongoose.model('LeadData', LeadSchema);
module.exports = LeadData;
