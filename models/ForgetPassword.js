const mongoose = require('mongoose');

const forgetPasswordSchema = new mongoose.Schema({
    guId: {
        type: String,
        required: true,
        unique: true
    },
    expire: {
        type: Boolean,
        default: false,
        unique: false
    },
    email: {
        type: String,
        required: true
    }
}, { timestamps: true })

const ForgetPassword = mongoose.model('ForgetPassword', forgetPasswordSchema);
module.exports = ForgetPassword