
const User = require('../models/User');

const bcrypt = require('bcryptjs');
let jwt = require('jsonwebtoken');
let config = require('../config');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
const ForgetPassword = require('../models/ForgetPassword');


exports.register = function (req, res) {
    if (!(req.body.password && req.body.email && req.body.name)) {
        return res.status(200).json({ success: 0, message: "Please fill all the field." });
    }

    else {
        User.findOne({ email: req.body.email }, (err, users) => {
            if (err) {

                return res.status(503).json({ success: 0, message: err.errmsg });
            } else if (Boolean(users)) {
                return res.status(200).json({ success: 0, message: "Email already exist." });
            } else {
                bcrypt.hash(req.body.password, 10, function (err, hash) {
                    if (err) {
                        return res.status(503).json({ success: 0, message: err.message });
                    } else {
                        var user = new User({
                            email: req.body.email.toLowerCase(),
                            name: req.body.name.toLowerCase(),
                            password: hash,
                            phone: req.body.phone,
                        })
                        user.save((error) => {

                            if (error) {
                                return res.status(503).json({ success: 0, message: error.errmsg });
                                // return res.json({ success: 1, message: "user already exist " });
                            }
                            let token = jwt.sign({ _id: user._id },
                                config.secret,
                                {
                                    expiresIn: '30d' // expires in 30 Days
                                }
                            );
                            res.status(200).json({ success: 1, message: 'Registration successful', data: user, token: token });
                        });
                    }

                })
            }
        })

    }
}



exports.login = function (req, res) {
    if (req.body.email && req.body.password) {
        User.authenticate(req.body.email, req.body.password, function (error, user) {
            if (error || !user) {
                res.status(200).send({ "success": 0, "message": "Email or password you have entered is invalid!" })
            } else {
                let token = jwt.sign({ _id: user._id },
                    config.secret,
                    {
                        expiresIn: '30d' // expires in 30 days
                    }
                );
                res.status(200).send({ "success": 1, "message": "Login Success", data: user, token: token })
            }
        });
    } else {
        res.status(200).send({ "success": 0, "message": "Email or password you have entered is invalid!" })
    }
}


// global Random String Generator
function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}


/**
 * Forget Password Send mail
 */
exports.forgetPassword = function (req, res, next) {

    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) {

            res.status(500).send({ success: 0, message: err.errmsg })

        } else if (!user) {
            res.status(200).send({ success: 0, message: "We could not find this email in our records." })

        } else {

            var guId = "ss-s-s-s-sss".replace(/s/g, s4);
            var passwordData = {
                guId: guId,
                expire: 0,
                email: req.body.email
            }

            ForgetPassword.create(passwordData, function (error, saved) {
                if (error) {
                    res.status(500).send({ success: 0, message: error.errmsg })
                    console.log(error)
                } else {
                    var options = {
                        auth: {
                            api_key: config.sendGridkey
                        }
                    }
                    const client = nodemailer.createTransport(sgTransport(options));

                    const email = {
                        from: 'TRYBEX',
                        to: req.body.email,
                        subject: 'Trybex - Password reset',
                        text: 'See below',
                        html: "<p>Please <a href='" + 'https://trybex-chat.herokuapp.com' + "/auth/reset-password/" + guId + "'>click here" + "</a> to reset your password.</p>"
                    };
                    // res.status(200).send({ success: 0,message:'A password reset link has been sent to your email.', url: req.protocol + '://' + req.get('host') + "/reset-password/" + guId })
                    client.sendMail(email, function (err, info) {
                        console.log(err)

                        if (err) {
                            console.log(err);
                            res.status(500).send({ success: 0, message: err.errmsg })

                        } else {
                            // console.log(JSON.stringify(info));
                            res.status(200).send({ success: 1, message: 'A password reset link has been sent to your email. Please open your Email box.' })
                        }
                    });
                }
            });
        }
    })
}


/**
 * Reset the forgot Password
 */
exports.resetPassword = function (req, res, next) {

    if (!req.body.guId || !req.body.password) {
        res.status(200).send({ success: 0, message: "Please Fill all the field" })
    }
    else {
        ForgetPassword.findOne({ guId: req.body.guId }, (err, obj) => {
            if (err) {
                console.log(err)
                res.status(500).send({ success: 0, message: err.errmsg })

            } else if (!obj) {
                res.status(200).send({ success: 0, message: "Your reset password link must have expired. Please try again." })

            } else if (obj.expire == 0) {
                res.status(200).send({ success: 0, message: "Your reset password link must have expired. Please try again." })

            }
            else {

                User.findOne({ email: obj.email }, function (err, user) {
                    if (err) {
                        res.status(500).send({ success: 0, message: err.errmsg })

                    } else {
                        if (user) {
                            bcrypt.hash(req.body.password, 10, function (err, hash) {
                                if (err) {
                                    return res.status(503).json({ success: 0, message: err.message });

                                } else {
                                    user.password = hash;

                                    user.save((err) => {
                                        if (err) {
                                            res.status(500).send({ success: 0, message: err.errmsg })
                                        } else {

                                            ForgetPassword.deleteOne({ email: obj.email }, function (err, result) {
                                                if (err) {
                                                    res.status(500).send({ success: 0, message: err.errmsg })

                                                } else {
                                                    res.status(200).send({ success: 1, message: "Your password has been reset." })
                                                }
                                            })
                                        }
                                    })
                                }
                            })

                        } else {
                            res.status(200).send({ success: 0, message: "Password change failed. Please try again." })
                        }
                    }
                })
            }
        })
    }
};

/**
 * Change Password
 */
exports.changePassword = function (req, res, next) {

    if (!req.body.oldPassword || !req.body.password) {
        res.status(200).send({ success: 0, message: "Please Fill all the field" })
    }
    else {
        User.findOne({ _id: req.decoded._id }, function (err, user) {
            if (err) {
                res.status(500).send({ success: 0, message: err.errmsg })

            } else {
                // Check for old password match
                User.authenticate(user.email, req.body.oldPassword, (callback, user) => {
                    if (callback) {
                        res.status(200).send({ "success": 0, "message": "you have entered wrong old Password!" })
                    } else {
                        if (user) {
                            bcrypt.hash(req.body.password, 10, function (err, hash) {
                                if (err) {
                                    return res.status(503).json({ success: 0, message: err.message });

                                } else {
                                    user.password = hash;
                                    user.save((err) => {
                                        if (err) {
                                            res.status(500).send({ success: 0, message: err.errmsg })
                                        } else {
                                            res.status(200).send({ success: 1, message: "Your password has been changed." })
                                        }
                                    })
                                }
                            })
                        } else {
                            res.status(200).send({ "success": 0, "message": "you have entered wrong old Password!" })
                        }
                    }
                })
            }
        })
    }
}

