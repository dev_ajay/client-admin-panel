const LeadData = require('../models/Leaddata');
var syncLoop = require('sync-loop');

/* GET Clients listing.   */
exports.clientsCount = function (req, res, next) {

    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "name";
    let skip = (filter.pageNo - 1) * itemPerPage;

    if (filter) {
        let search = filter.where.toLowerCase()
        LeadData.count({
            $and: [{ status: 'client' }, {
                $or: [
                    { "name": { "$regex": search, "$options": "i" } },
                    { "email": { "$regex": search, "$options": "i" } },
                ]
            }]
        }).exec((error, users) => {
            if (error) {
                return res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                return res.status(200).send({ "success": 1, message: "LeadData list Count", data: users });
            }
        })
    } else {
        LeadData.count({ status: 'client' }).exec((error, users) => {
            if (error) {
                return res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                return res.status(200).send({ "success": 1, message: "LeadData list Count", data: users });
            }
        })
    }
};


/* GET users listing.   */
exports.clients = function (req, res, next) {

    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "name";
    let skip = (filter.pageNo - 1) * itemPerPage;

    if (filter) {
        let search = filter.where.toLowerCase()
        LeadData.find({
            $and: [{ status: 'client' }, {
                $or: [
                    { "name": { "$regex": search, "$options": "i" } },
                    { "email": { "$regex": search, "$options": "i" } },
                ]
            }]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                return res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                return res.status(200).send({ "success": 1, message: "Clients list", data: users });
            }
        })
    } else {
        LeadData.find({ status: 'client' }).limit(itemPerPage).exec((error, users) => {
            if (error) {
                return res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                return res.status(200).send({ "success": 1, message: "LeadData lists", data: users });
            }
        })
    }
};


/* GET  inprogress Clients listing.   */
exports.clientsInProgressCount = function (req, res, next) {

    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "createdAt";
    let skip = (filter.pageNo - 1) * itemPerPage;

    if (filter) {
        let search = filter.where.toLowerCase()
        LeadData.count({
            $and: [{ status: 'inprogress' }, {
                $or: [
                    { "name": { "$regex": search, "$options": "i" } },
                    { "email": { "$regex": search, "$options": "i" } },
                ]
            }]
        }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "InProgressClient list Count", data: users });
            }
        })
    } else {
        LeadData.count({ status: 'inprogress' }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "InProgressClient list Count", data: users });
            }
        })
    }
};


/* GET users listing.   */
exports.clientsInProgress = function (req, res, next) {

    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "createdAt";
    let skip = (filter.pageNo - 1) * itemPerPage;

    if (filter) {
        let search = filter.where.toLowerCase()
        LeadData.find({
            $and: [{ status: 'inprogress' }, {
                $or: [
                    { "name": { "$regex": search, "$options": "i" } },
                    { "email": { "$regex": search, "$options": "i" } },
                ]
            }]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "Clients list", data: users });
            }
        })
    } else {
        LeadData.find({ status: 'inprogress' }).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "InProgressClient lists", data: users });
            }
        })
    }
};


/* GET final clients listing.   */
exports.clientsFinalCount = function (req, res, next) {

    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "createdAt";
    let skip = (filter.pageNo - 1) * itemPerPage;

    if (filter) {
        let search = filter.where.toLowerCase()
        LeadData.count({
            $and: [{ status: 'final' }, {
                $or: [
                    { "name": { "$regex": search, "$options": "i" } },
                    { "email": { "$regex": search, "$options": "i" } },
                ]
            }]
        }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "FinalClient list Count", data: users });
            }
        })
    } else {
        LeadData.count({ status: 'final' }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "FinalClient list Count", data: users });
            }
        })
    }
};


/* GET final users listing.   */
exports.clientsFinal = function (req, res, next) {

    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "createdAt";
    let skip = (filter.pageNo - 1) * itemPerPage;

    if (filter) {
        let search = filter.where.toLowerCase()
        LeadData.find({
            $and: [{ status: 'final' }, {
                $or: [
                    { "name": { "$regex": search, "$options": "i" } },
                    { "email": { "$regex": search, "$options": "i" } },
                ]
            }]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "Clients list", data: users });
            }
        })
    } else {
        LeadData.find({ status: 'final' }).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "FinalClient lists", data: users });
            }
        })
    }
};


exports.createClient = function (req, res) {
    if (!(req.body.email)) {
        return res.status(200).json({ success: 0, message: "Please fill all the field." });
    }
    else {
        LeadData.findOne({ email: req.body.email }, (err, users) => {
            if (err) {
                return res.status(503).json({ success: 0, message: err.errmsg });
            } else if (Boolean(users)) {
                return res.status(200).json({ success: 0, message: "Email already exist." });
            } else {

                var user = new LeadData({
                    email: req.body.email.toLowerCase(),
                    name: req.body.name.toLowerCase(),
                    phone: req.body.phone,
                    course: req.body.course,
                    date: req.body.date,
                    dob: req.body.dob,
                    campaign_name: req.body.campaign_name,
                    platform: req.body.platform,
                    feedback: req.body.feedback,
                });
                user.save((error) => {

                    if (error) {
                        return res.status(503).json({ success: 0, message: error.errmsg });
                        // return res.json({ success: 1, message: "user already exist " });
                    }

                    res.status(200).json({ success: 1, message: 'successful', data: user });
                });

            }
        })

    }
}



exports.createClientArray = function (req, res) {
    let allClients = req.body.clients;
    console.log(allClients);
    syncLoop(allClients.length, function (loop) {
        var index = loop.iteration(); // index of loop, value from 0 to (numberOfLoop - 1)
        if (!(allClients[index].email)) {
            loop.next();
        }
        else {
            LeadData.findOne({ email: allClients[index].email }, (err, users) => {
                if (err) {
                    return res.status(503).json({ success: 0, message: err.errmsg });
                } else if (Boolean(users)) {
                    loop.next();
                } else {

                    var user = new LeadData({
                        email: allClients[index].email ? allClients[index].email.toLowerCase() : '',
                        name: allClients[index].name ? allClients[index].name.toLowerCase() : '',
                        phone: allClients[index].phone,
                        course: allClients[index].course,
                        date: allClients[index].date,
                        dob: allClients[index].dob,
                        campaign_name: allClients[index].campaign_name,
                        platform: allClients[index].platform,
                        feedback: allClients[index].feedback,
                    });
                    user.save((error) => {

                        if (error) {
                            loop.next();
                            // return res.status(503).json({ success: 0, message: error.errmsg });
                            // return res.json({ success: 1, message: "user already exist " });
                        }
                        loop.next();

                        // res.status(200).json({ success: 1, message: 'successful', data: user });
                    });

                }
            })

        }

        // Like.findOne({ postId: allPosts[index]._id }, (err, like) => {
        //     if (err || !like) {
        //         loop.next();
        //     } else {
        //         let indexs = -1;
        //         indexs = like.userId.indexOf(req.decoded._id);
        //         if (indexs > -1) {
        //             allPosts[index]['isLike'] = true;
        //             loop.next();
        //         } else {
        //             loop.next();
        //         }
        //     }
        // })


    }, function () {
        return res.status(200).json({ success: 1, message: "user all data", data: allClients });
    });

}

exports.createInProgressClient = function (req, res) {
    if (!(req.body.email)) {
        return res.status(200).json({ success: 0, message: "Please fill all the field." });
    }
    else {
        LeadData.findOne({ email: req.body.email }, (err, users) => {
            if (err) {
                return res.status(503).json({ success: 0, message: err.errmsg });
            } else if (!Boolean(users)) {
                return res.status(200).json({ success: 0, message: "User does not exist." });
            } else {

                users.fee = req.body.fee;
                users.scheduleCall = req.body.scheduleCall;
                users.dateOfJoining = req.body.dateOfJoining;
                users.details = req.body.details;
                users.status = 'inprogress';
                users.save((error) => {

                    if (error) {
                        return res.status(503).json({ success: 0, message: error.errmsg });
                        // return res.json({ success: 1, message: "user already exist " });
                    }

                    res.status(200).json({ success: 1, message: 'successful', data: users });
                });

            }
        })

    }
}


exports.updateInProgressClient = function (req, res) {

    LeadData.findOne({ _id: req.body._id }, (err, user) => {
        if (err) {
            return res.status(503).json({ success: 0, message: err.errmsg });
        } else if (!Boolean(user)) {
            return res.status(200).json({ success: 0, message: "User does not exist." });
        } else {
            user.fee = req.body.fee || user.fee;
            user['dateOfJoining'] = req.body.dateOfJoining || user.dateOfJoining;
            user['scheduleCall'] = req.body.scheduleCall || user.scheduleCall;
            user['details'] = req.body.details || user.details;

            user.save((error) => {

                if (error) {
                    return res.status(503).json({ success: 0, message: error.errmsg });
                }

                res.status(200).json({ success: 1, message: 'successful', data: user });
            });

        }
    })


}

exports.createFinalClient = function (req, res) {
    if (!(req.body.email)) {
        return res.status(200).json({ success: 0, message: "Please fill all the field." });
    }
    else {
        LeadData.findOne({ email: req.body.email }, (err, users) => {
            if (err) {
                return res.status(503).json({ success: 0, message: err.errmsg });
            } else if (!Boolean(users)) {
                return res.status(200).json({ success: 0, message: "User does already exist." });
            } else {
                users.fee = req.body.fee;
                users.scheduleCall = req.body.scheduleCall;
                users.dateOfJoining = req.body.dateOfJoining;
                users.details = req.body.details;
                users.status = 'final';
                users.save((error) => {
                    if (error) {
                        return res.status(503).json({ success: 0, message: error.errmsg });
                    }
                    res.status(200).json({ success: 1, message: 'successful', data: users });
                });

            }
        })

    }
}
/* GET users Details. */
exports.clientDetails = function (req, res, next) {

    LeadData.findOne({ _id: req.params.id }).exec((error, user) => {
        if (error) {
            console.log(error)
            return res.status(500).send({ "success": 0, "message": error.errmsg })
        } else {
            return res.status(200).send({ "success": 1, message: "FinalClient details", data: user });
        }
    })
}

/* Delete LeadData Details. */
exports.deleteClient = function (req, res, next) {

    LeadData.findOneAndRemove({ _id: req.params.id }).exec((error) => {
        if (error) {
            console.log(error)
            return res.status(500).send({ "success": 0, "message": error.errmsg })
        } else {
            return res.status(200).send({ "success": 1, message: "Final LeadData removed" });
        }
    })
}


