const User = require('../models/User');
const bcrypt = require('bcryptjs');
let jwt = require('jsonwebtoken');
let config = require('../config');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
const ForgetPassword = require('../models/ForgetPassword');

/* GET users listing.   */
exports.usersList = function (req, res, next) {
    User.find({ $or: [{ role: 'student' }, { role: null }] }).exec((error, users) => {
        if (error) {
            console.log(error)
            res.status(500).send({ "success": 0, "message": error.errmsg })
        } else {
            res.status(200).send({ "success": 1, message: "User list success", data: users });
        }
    })
};

/* GET users listing total Count  */
exports.getUnapprovedListCount = function (req, res, next) {
    let allRoles = req.app.get('roles');
    let index = allRoles.indexOf(req.decoded.role);
    let rolesList = [];
    for (let i = index + 1; i < allRoles.length; i++) {
        rolesList.push({ role: allRoles[i] });
    }
    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "name";
    let skip = (filter.pageNo - 1) * itemPerPage;
    if (filter) {
        let search = filter.where.toLowerCase()
        User.count({
            $and: [
                { approved: false },
                { $or: rolesList },
                {
                    $or: [
                        { "name": { "$regex": search, "$options": "i" } },
                        { "email": { "$regex": search, "$options": "i" } },
                    ]
                },
                { college: req.decoded.college }
            ]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User list Count", data: users });
            }
        })
    } else {
        User.count({
            $and: [
                { approved: false },
                { $or: rolesList },
                { college: req.decoded.college }
            ]
        }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User list success", data: users });
            }
        })
    }

};



/* GET users listing.   */
exports.unapprovedList = function (req, res, next) {
    let allRoles = req.app.get('roles');
    let index = allRoles.indexOf(req.decoded.role);
    let rolesList = [];
    for (let i = index + 1; i < allRoles.length; i++) {
        rolesList.push({ role: allRoles[i] });
    }
    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "name";
    let skip = (filter.pageNo - 1) * itemPerPage;
    if (filter) {
        let search = filter.where.toLowerCase()
        User.find({
            $and: [
                { approved: false },
                { $or: rolesList },
                {
                    $or: [
                        { "name": { "$regex": search, "$options": "i" } },
                        { "username": { "$regex": search, "$options": "i" } },
                    ]
                },
                { college: req.decoded.college }
            ]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User list success", data: users });
            }
        })
    } else {
        User.find({
            $and: [
                { approved: false },
                { $or: rolesList },
                { college: req.decoded.college }
            ]
        }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User list success", data: users });
            }
        })
    }

};



/* GET Approved users listing total Count  */
exports.getApprovedListCount = function (req, res, next) {
    let allRoles = req.app.get('roles');
    let index = allRoles.indexOf(req.decoded.role);
    let rolesList = [];
    for (let i = index + 1; i < allRoles.length; i++) {
        rolesList.push({ role: allRoles[i] });
    }
    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "name";
    let skip = (filter.pageNo - 1) * itemPerPage;
    if (filter) {
        let search = filter.where.toLowerCase()
        User.count({
            $and: [
                { approved: true },
                { $or: rolesList },
                {
                    $or: [
                        { "name": { "$regex": search, "$options": "i" } },
                        { "email": { "$regex": search, "$options": "i" } },
                    ]
                },
                { college: req.decoded.college }
            ]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User Approved list success", data: users });
            }
        })
    } else {
        User.count({
            $and: [
                { approved: true },
                { $or: rolesList },
                { college: req.decoded.college }
            ]
        }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User Approved list success", data: users });
            }
        })
    }

};



/* GET Aproves users listing.   */
exports.approvedList = function (req, res, next) {
    let allRoles = req.app.get('roles');
    let index = allRoles.indexOf(req.decoded.role);
    let rolesList = [];
    for (let i = index + 1; i < allRoles.length; i++) {
        rolesList.push({ role: allRoles[i] });
    }
    let filter = JSON.parse(req.query.filter)
    let itemPerPage = filter.itemPerPage ? filter.itemPerPage : 15;
    let sort = filter.sort ? filter.sort : "name";
    let skip = (filter.pageNo - 1) * itemPerPage;
    if (filter) {
        let search = filter.where.toLowerCase()
        User.find({
            $and: [
                { approved: true },
                { $or: rolesList },
                {
                    $or: [
                        { "name": { "$regex": search, "$options": "i" } },
                        { "username": { "$regex": search, "$options": "i" } },
                    ]
                },
                { college: req.decoded.college }
            ]
        }).sort(sort).skip(skip).limit(itemPerPage).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User Approved list success", data: users });
            }
        })
    } else {
        User.find({
            $and: [
                { approved: true },
                { $or: rolesList },
                { college: req.decoded.college }
            ]
        }).exec((error, users) => {
            if (error) {
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, message: "User Approved list success", data: users });
            }
        })
    }

};

/* GET users listing. */
exports.me = function (req, res, next) {

    User.findOne({ _id: req.decoded._id }).exec((error, user) => {
        if (error) {
            console.log(error)
            res.status(500).send({ "success": 0, "message": error.errmsg })
        } else if (user) {
            res.status(200).send({ "success": 1, message: "User details", data: user });
        } else {
            res.status(200).send({ "success": 0, "message": "User does not exist" })

        }
    })
};

/* GET users Details. */
exports.userDetails = function (req, res, next) {

    User.findOne({ _id: req.params.id }).exec((error, user) => {
        if (error) {
            console.log(error)
            return res.status(500).send({ "success": 0, "message": error.errmsg })
        } else {
            return res.status(200).send({ "success": 1, message: "User details", data: user });
        }
    })

}

/* Update users Details. */
exports.updateUser = function (req, res, next) {

    if (req.decoded._id == req.params.id) {
        User.findById({ _id: req.decoded._id }).exec((error, user) => {
            if (error) {
                console.log(error)
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
            
                user.name = req.body.name.toLowerCase() || user.name;
                user.phone = req.body.phone || user.phone;

                user.save((err) => {
                    if (err) {
                        res.status(500).send({ "success": 0, "message": err.errmsg })
                    } else {
                        res.status(200).send({ "success": 1, data: user,  message: "User profile updated." });
                    }
                })
            }
        })
    } else {

        return res.status(403).send({
            success: 0,
            message: 'User is not authorized.'
        })
    }

}

exports.searchOnlyUsers = function (req, res, next) {

    console.log("search value: " + req.query.search)
    if (req.query.search && req.query.search != "") {
        let search = req.query.search.toLowerCase();
        User.find({
            $and: [
                { _id: { "$ne": req.decoded._id } },
                {
                    $or: [
                        { role: null },
                        { role: 'student' }]
                },
                {
                    $or: [
                        {
                            "name": { "$regex": search, "$options": "i" }
                        },
                        { "username": { "$regex": search, "$options": "i" } }
                    ]
                },


            ]
        }).exec((error, users) => {
            if (error) {
                // console.log(error)
                res.status(500).send({ "success": 0, "message": error.errmsg })
            } else {
                res.status(200).send({ "success": 1, data: users });
            }
        })
    } else {
        res.status(200).send({ "success": 1, data: [] });

    }

}

exports.searchAll = function (req, res, next) {

    let search = req.params.search.toLowerCase()

    User.find({
        $and: [
            { _id: { "$ne": req.decoded._id } },
            {
                $or: [
                    {
                        "name": { "$regex": search, "$options": "i" }
                    },
                    { "username": { "$regex": search, "$options": "i" } },

                ]
            }]

    }).exec((error, users) => {
        if (error) {
            // console.log(error)
            res.status(500).send({ "success": 0, "message": error.errmsg })
        } else {
            res.status(200).send({ "success": 1, data: users });
        }
    })
}


