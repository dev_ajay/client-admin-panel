const User = require('../models/User');
const config = require('../config')
let jwt = require('jsonwebtoken');


function isAuthenticated(req, res, next) {
  next();

  // let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
  // if (token && token.startsWith('Bearer ')) {
  //   // Remove Bearer from string
  //   token = token.slice(7, token.length);
  // }
  // if (token) {
  //   jwt.verify(token, config.secret, (err, decoded) => {
  //     if (err) {
  //       return res.json({
  //         success: 0,
  //         message: 'Token is not valid'
  //       });
  //     } else {
  //       req.decoded = decoded;
  //       next();
  //     }
  //   });
  // } else {
  //   return res.json({
  //     success: 0,
  //     message: 'Auth token is not supplied'
  //   });
  // }
}
module.exports.isAuthenticated = isAuthenticated;
