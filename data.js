const User = require('./models/User');
// Database connection
const fs = require('fs');
const bcrypt = require('bcryptjs');

const mongoose = require('mongoose');
// const mongoUrl = 'mongodb://admin:admin1234@ds139956.mlab.com:39956/levelupthechill';
// const mongoUrl ='mongodb://dev:developer1@ds253537.mlab.com:53537/trybex-temp';
const mongoUrl ='mongodb://dev:developer1@ds259596.mlab.com:59596/trybex-dev';

mongoose.connect(mongoUrl, { useNewUrlParser: true }).then(() => {
    console.log("Connected to Database");
    bcrypt.hash(process.env.adminPassword || 'admin@123', 10, function (err, hash) {
        if (err) {
            exit();

            return res.status(503).json({ success: 0, message: error.message });

        } else {
            User.create({
                email: process.env.adminEmail || 'admin@gmail.com',
                password: hash, role: 'super_admin'
            }, function (err, newProduct) {
                if (err) {
                    console.log(err);
                    exit();
                } else {
                    console.log("Admin is created.");
                    exit();
                }
            });
        }
    })

    function exit() {
        mongoose.disconnect();
    }

}).catch((err) => {
    console.log("Not Connected to Database ERROR! ", err);
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));



