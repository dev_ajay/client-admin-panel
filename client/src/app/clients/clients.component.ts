import { Component, OnInit, TemplateRef, AfterViewInit } from '@angular/core';
import { ApiService } from '../api.service';

import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import * as XLSX from 'xlsx';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'ngx-clients-table',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientComponent implements OnInit, AfterViewInit {
  pageCategory: string = 'client';
  modalRef: BsModalRef;
  users: any[] = [];
  data: any[] = [];
  isElementLoading = true;
  modelChanged: Subject<string> = new Subject<string>();
  selectedClient: any;
  filter = {
    where: '',
    sort: 'name',
    itemPerPage: 15,
    pageNo: 1
  };
  deleteDialogRef: BsModalRef;
  min: Date;
  clientForm: FormGroup;
  totalItems = 10;
  constructor(
    private api: ApiService,
    private modalService: BsModalService
    // private dialogService: NbDialogService,
    // protected dateService: NbDateService<Date>
  ) {
    // this.min = this.dateService.addDay(this.dateService.today(), 0);
  }

  ngOnInit() {
    this.getList()


    this.modelChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(model => {
        this.getList()
      });

    this.clientForm = new FormGroup({
      fee: new FormControl(''),
      scheduleCall: new FormControl(''),
      dateOfJoining: new FormControl(''),
      details: new FormControl('')
    });
  }

  ngAfterViewInit() {
    const input: any = document.getElementById('input')

    // input.addEventListener('change', () => {
    //   readXlsxFile(input.files[0]).then((rows) => {
    //     // `rows` is an array of rows
    //     // each row being an array of cells.
    //     console.log(rows)
    //   })
    // })
  }

  /**
   * Switchs category
   * @param event 
   */
  switchCategory(event) {
    this.pageCategory = event.target.value;
    this.getList();
  }

  /**
   * Gets list according to filters
   */
  getList() {
    switch (this.pageCategory) {
      case 'client':
        this.getClientList();
        break;
      case 'inprogress':
        this.getInprogressList();
        break;
      case 'final':
        this.getFinalList();
        break;
    }
  }

  /**
  * Gets list according to filters
  */
  getNextList(event) {
    switch (this.pageCategory) {
      case 'client':
        this.getNextClientList(event);
        break;
      case 'inprogress':
        this.getInprogressNextList(event);
        break;
      case 'final':
        this.getFinalNextList(event);
        break;
    }
  }
  /**
   * Determines whether file change on
   * @param evt 
   */
  onFileChange(evt: any) {
    /* wire up file reader */
    this.data = [];
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = XLSX.utils.sheet_to_json(ws, { header: 1 });
    };
    reader.readAsBinaryString(target.files[0]);
  }


  /**
   * Opens details
   * @param dialog 
   * @param user 
   */
  openDetails(dialog: TemplateRef<any>, user) {
    this.modalRef = this.modalService.show(dialog, { class: 'modal-lg' });
    this.selectedClient = user;
    this.clientForm.controls['fee'].setValue(user.fee);
    this.clientForm.controls['scheduleCall'].setValue(user.scheduleCall ? new Date(user.scheduleCall) : '');
    this.clientForm.controls['dateOfJoining'].setValue(user.dateOfJoining ? new Date(user.dateOfJoining) : '');
    this.clientForm.controls['details'].setValue(user.details);
  }

  searchChangeEvent(event) {
    this.modelChanged.next(event.target.value);
  }


  saveToDB() {
    var final = [];
    let obj = {};

    for (let i = 0; i < this.data.length; i++) {
      let row = this.data[i];
      let index = 0;
      obj = {};
      for (let k = 0; k < this.data.length; k++) {
        let col = row[k];
        obj[this.data[0][index++]] = col;
      }
      final.push(obj);
    }
    final.splice(0, 1)
    this.data = [];
    this.api.post('/clients/array', { clients: final }).subscribe((res) => {
      this.getList();
    })

  }

  cancelSaveToDB() {
    this.data = [];
  }

  /**
   * Gets client list
   */
  getClientList() {
    this.filter.pageNo = 1;
    this.api.get('/clients' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      this.isElementLoading = false;
      if (res.success) {
        this.users = res.data;
      } else {
        this.users = []
      }
    },
      err => {
        this.isElementLoading = false;
        console.log(err)
      })

    this.api.get('/clients/count' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      if (res.success) {
        this.totalItems = res.data;
      } else {
        this.totalItems = 0
      }
    },
      err => {
        console.log(err)
      })

  }

  /**
   * Gets next client list
   * @param event 
   */
  getNextClientList(event) {
    this.filter.pageNo = event.page;
    this.api.get('/clients' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      if (res.success) {
        this.users = res.data;
      } else {
        this.users = []
      }
    },
      err => {
        console.log(err)
      })
  }

  approveUser(id) {
    this.api.patch('/users/approve/' + id, {}).subscribe(res => {
      let index = this.users.findIndex(x => x._id === id);
      this.users.splice(index, 1)
    },
      err => {
        console.log(err)
      })
  }

  saveUser() {
    this.modalRef.hide();
    let user = this.selectedClient;
    user['fee'] = this.clientForm.value.fee;
    user['dateOfJoining'] = this.clientForm.value.dateOfJoining;
    user['scheduleCall'] = this.clientForm.value.scheduleCall;
    user['details'] = this.clientForm.value.details;

    if (user.scheduleCall) {
      this.api.post('/clients/inprogress', user).subscribe(res => {
        let index = this.users.findIndex(x => x._id === this.selectedClient._id);
        this.users.splice(index, 1);

      },
        err => {
          console.log(err)
        })
    } else {
      this.api.post('/clients/final', user).subscribe(res => {
        let index = this.users.findIndex(x => x._id === this.selectedClient._id);
        this.users.splice(index, 1);

      },
        err => {
          console.log(err)
        })
    }

  }

  /**
   * Deletes user
   * @param dialogname 
   * @param user 
   */
  deleteUser(dialogname, user) {
    this.selectedClient = user;
    this.deleteDialogRef = this.modalService.show(dialogname);
  }

  /**
   * Deletes confirm
   */
  deleteConfirm() {
    this.api.delete('/clients/' + this.selectedClient._id).subscribe(res => {
      let index = this.users.findIndex(x => x._id === this.selectedClient._id);
      this.users.splice(index, 1);
      this.deleteDialogRef.hide();
    },
      err => {
        console.log(err)
      })
  }

  /**
   * Deletecancels client component
   */
  deletecancel() {
    this.deleteDialogRef.hide();
    this.selectedClient = undefined;
  }


  /**
   * Sends to final
   */
  sendToFinal() {
    let user = this.selectedClient;
    user['fee'] = this.clientForm.value.fee;
    user['dateOfJoining'] = this.clientForm.value.dateOfJoining;
    user['scheduleCall'] = this.clientForm.value.scheduleCall;
    user['details'] = this.clientForm.value.details;

    this.api.post('/clients/final', user).subscribe(res => {
      let index = this.users.findIndex(x => x._id === this.selectedClient._id);
      this.users.splice(index, 1);
      this.modalRef.hide();
    },
      err => {
        console.log(err)
      })
  }




  updateUser() {
    let user = this.selectedClient;
    user['fee'] = this.clientForm.value.fee;
    user['dateOfJoining'] = this.clientForm.value.dateOfJoining;
    user['scheduleCall'] = this.clientForm.value.scheduleCall;
    user['details'] = this.clientForm.value.details;

    this.api.put('/clients/inprogress', user).subscribe(res => {
      let index = this.users.findIndex(x => x._id === this.selectedClient._id);
      this.users[index] = res.data;
      this.modalRef.hide();
    },
      err => {
        console.log(err)
      })
  }

  /**
   * Gets inprogress list
   */
  getInprogressList() {
    this.filter.pageNo = 1;
    this.api.get('/clients/inprogress' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      this.isElementLoading = false;
      if (res.success) {
        this.users = res.data;
      } else {
        this.users = []
      }
    },
      err => {
        this.isElementLoading = false;
        console.log(err)
      })

    this.api.get('/clients/inprogress/count' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      if (res.success) {
        this.totalItems = res.data;
      } else {
        this.totalItems = 0
      }
    },
      err => {
        console.log(err)
      })

  }

  /**
   * Gets inprogress next list
   * @param event 
   */
  getInprogressNextList(event) {
    this.filter.pageNo = event.page;
    this.api.get('/clients/inprogress' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      if (res.success) {
        this.users = res.data;
      } else {
        this.users = []
      }
    },
      err => {
        console.log(err)
      })
  }

  /**
   * Gets final list
   */
  getFinalList() {
    this.filter.pageNo = 1;
    this.api.get('/clients/final' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      this.isElementLoading = false;
      if (res.success) {
        this.users = res.data;
      } else {
        this.users = []
      }
    },
      err => {
        this.isElementLoading = false;
        console.log(err)
      })

    this.api.get('/clients/final/count' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      if (res.success) {
        this.totalItems = res.data;
      } else {
        this.totalItems = 0
      }
    },
      err => {
        console.log(err)
      })

  }

  /**
   * Gets final next list
   * @param event 
   */
  getFinalNextList(event) {
    this.filter.pageNo = event.page;
    this.api.get('/clients/final' + '?filter=' + JSON.stringify(this.filter)).subscribe(res => {
      if (res.success) {
        this.users = res.data;
      } else {
        this.users = []
      }
    },
      err => {
        console.log(err)
      })
  }


}
