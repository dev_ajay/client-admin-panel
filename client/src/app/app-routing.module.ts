import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './clients/clients.component';
import { CalldataComponent } from './calldata/calldata.component';
import { LeaddataComponent } from './leaddata/leaddata.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/walkins',
    pathMatch: 'full'
  },
  {
    path: 'walkins',
    pathMatch: 'full',
    component: ClientComponent,
  },
  {
    path: 'incoming-calls',
    component: CalldataComponent,
    pathMatch: 'full'
  },
  {
    path: 'lead-gen',
    component: LeaddataComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  static component = [ClientComponent, CalldataComponent, LeaddataComponent];
}
